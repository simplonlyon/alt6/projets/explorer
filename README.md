# File explorer

Ce projet va permettre de s'entrainer au travail de groupe et à la POO en se servant d'une application Desktop en JavaFX comme support.

## Objectif
Par groupe de 3 personnes, créer un explorateur de fichiers qui permettra de naviguer sur l'appareil et d'interagir avec dossiers et fichiers (création, suppression, modification, propriétés)

## Réalisation
L'idée sera de séparer le plus possible la couche affichage du reste en faisant des classes et méthodes complètement séparés de celui-ci. (Dans l'idéal si l'on décidait de faire un CLI plutôt qu'une interface graphique, toutes les fonctionnalités n'aurait pas besoin d'être modifiées)

Faire également en sorte de tester unitairement toutes les méthodes créées hors affichage.

Enfin, pour l'organisation du travail en équipe, créer un dépôt Gitlab et faire que chaque membre du groupe code sur des branches de développement en gardant la main propre (on merge sur la main lorsqu'une fonctionnalité est terminée et testée)

### Fonctionnalités sans interface graphique ?!
Oui, ça va sans doute impliquer un peu de répétition par rapport aux classes propres de java Files, mais parfois non et l'exercice est intéressant.

**Exemples de classes possibles**

Pourquoi pas une classe "principale" qui va représenter l'état de navigation actuel
```java
class ExplorerState {
    private String currentLocation = "/";

    //...

    public void navigate(String folder) {}

    public void navigate(String folder, boolean absolute) {}

    //...
}

```

Pourquoi pas une classe utilitaire pour lister et récupérer les propriétés d'un chemin (qui pourrait être utilisé par le ExplorerState ou autre)

```java
class FileLister {

    public List<String> list(String folder) {
        //...
    }

    public BasicFileAttributes getProperties(String path) {
        //...
    }
}

```

Et dans un second temps, créer des classes JavaFX utilisant les classes définies et assigner au click sur les button et composant d'interface les méthodes qu'on aura définies

(Ne suivez pas ces exemples particulièrement, c'est juste une idée, pas forcément l'unique ou la meilleure)
